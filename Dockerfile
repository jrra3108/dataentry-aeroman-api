FROM node:lts-jessie-slim

RUN mkdir -p opt/oracle
COPY ./oracle/linux/instantclient-basic-linux.x64-12.2.0.1.0.zip .
COPY ./oracle/linux/instantclient-sdk-linux.x64-12.2.0.1.0.zip .

RUN apt-get update \
 && apt-get install -y --no-install-recommends unzip curl nano apt-utils libaio1 \
 && unzip instantclient-basic-linux.x64-12.2.0.1.0.zip -d /opt/oracle \
 && unzip instantclient-sdk-linux.x64-12.2.0.1.0.zip -d /opt/oracle \
 && mv /opt/oracle/instantclient_12_2 /opt/oracle/instantclient \
 && ln -s /opt/oracle/instantclient/libclntsh.so.12.2 /opt/oracle/instantclient/libclntsh.so \
 && ln -s /opt/oracle/instantclient/libocci.so.12.2 /opt/oracle/instantclient/libocci.so \
 && rm -rf /var/lib/apt/lists/*

RUN echo '/opt/oracle/instantclient/' | tee -a /etc/ld.so.conf.d/oracle_instant_client.conf && ldconfig

WORKDIR /usr/src/app

COPY package.json .
COPY ./dist/ .
RUN npm install --production

ENV LD_LIBRARY_PATH="/opt/oracle/instantclient"
ENV OCI_HOME="/opt/oracle/instantclient"
ENV OCI_LIB_DIR="/opt/oracle/instantclient"
ENV OCI_INCLUDE_DIR="/opt/oracle/instantclient/sdk/include"
ENV OCI_VERSION=12

EXPOSE 3000
CMD [ "node", "main" ]


