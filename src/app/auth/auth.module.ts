import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './service/jwt.strategy';
import { AuthController } from './controller/auth.controller';
import { AuthService } from './service/auth.service';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: 'aeroman2019',
    }),
  ],
  controllers: [
    AuthController,
  ],
  providers: [
    JwtStrategy,
    AuthService,
  ],
})
export class AuthModule {
}
