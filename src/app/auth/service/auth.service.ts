import { Inject, Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { LoginRequest } from '../dto/login.request';
import { Users } from '../../database/entities/user';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {

  constructor(
    @Inject('USER_REPOSITORY') private readonly repositorio: Repository<Users>,
    private readonly jwtService: JwtService,
  ) {}

  async createToken(request: LoginRequest) {
    const userExist: Users = await this.validateUser(request);
    if (userExist) {
      const payload = { id: userExist.id, username: userExist.username };
      const accessToken = this.jwtService.sign(payload, { expiresIn: 3600 });
      const expiresIn = 3600;

      return { expiresIn, accessToken };
    }
    return userExist;
  }

  private async validateUser(userData: LoginRequest): Promise<Users> {
    return await this.repositorio.createQueryBuilder('user')
      .where('user.username = :name', { name: userData.username })
      .getOne();
  }

}
