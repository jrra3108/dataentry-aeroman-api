import { ApiUseTags } from '@nestjs/swagger';
import { Controller, Post, Body } from '@nestjs/common';
import { LoginRequest } from '../dto/login.request';
import { AuthService } from '../service/auth.service';

@ApiUseTags('Inicio de Sesión')
@Controller('v1/auth')
export class AuthController {

  constructor(private readonly authTokenService: AuthService) {
  }

  @Post()
  async login(@Body() request: LoginRequest) {
    return this.authTokenService.createToken(request).then(token => {
      return token;
    }).catch( err => {
      return err;
    });
  }

}
