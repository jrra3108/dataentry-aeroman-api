import { Inject, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CriticalPaths } from '../../../../database/entities/critical-paths';
import { CreateCritical } from '../dto/create-critical';

@Injectable()
export class CriticalService {
  constructor(@Inject('CRITICAL_REPOSITORY') private readonly repositorio: Repository<CriticalPaths>) {
  }

  async create(registro: CreateCritical, createBy: string) {

    return await this.repositorio.createQueryBuilder().insert().into(CriticalPaths).values([
      {
        aircraft: registro.aircraft,
        workOrder: registro.workOrder,
        criticalPath: registro.criticalPath,
        tatImpact: registro.tatImpact,
        mitigationPlan: registro.mitigationPlan,
        responsible: registro.responsible,
        status: registro.status,
        statusFinal: registro.statusFinal,
        createdBy: createBy,
      },
    ]).execute().then(() => {
      return {
        code: 0,
        description: 'Critical creada con éxito',
      };
    }).catch(error => {
      Logger.verbose(`error => ${error}`);
      let descError;
      switch (error.errorNum) {
        case 1:
          descError = 'Critical ya existe.';
          break;
        case 1400:
          descError = 'No se pudo registrar, complete la información';
          break;
        default:
          descError = 'Error no determinado, consulte con el administrador';
          break;
      }
      return {
        code: error.errorNum,
        description: descError,
      };
    });
  }

  async update(registro: CreateCritical, updateBy: string) {

    return await this.repositorio.createQueryBuilder().update(CriticalPaths).set(
      {
        aircraft: registro.aircraft,
        workOrder: registro.workOrder,
        criticalPath: registro.criticalPath,
        tatImpact: registro.tatImpact,
        mitigationPlan: registro.mitigationPlan,
        responsible: registro.responsible,
        status: registro.status,
        statusFinal: registro.statusFinal,
        UpdatedBy: updateBy,
      },
    ).where('id = :id', { id: registro.id }).execute().then(() => {
      return {
        code: 0,
        description: 'Critical actualizado con éxito',
      };
    }).catch(error => {
      Logger.verbose(`error => ${error}`);
      let descError;
      switch (error.errorNum) {
        case 1:
          descError = 'La empresa ya existe.';
          break;
        case 1400:
          descError = 'No se pudo registrar el Critical, complete la información';
          break;
        default:
          descError = 'Error no determinado, consulte con el administrador';
          break;
      }
      return {
        code: error.errorNum,
        description: descError,
      };
    });
  }

  async delete(idrecord: string) {

    return await this.repositorio.createQueryBuilder().delete().from(CriticalPaths)
      .where('id = :id', { id: idrecord }).execute().then(() => {
        return {
          code: 0,
          description: 'Empresa actualizada con éxito',
        };
      }).catch(error => {
        Logger.verbose(`error => ${error}`);
        let descError;
        switch (error.errorNum) {
          case 1:
            descError = 'La empresa ya existe.';
            break;
          case 1400:
            descError = 'No se pudo registrar la empresa, complete la información';
            break;
          default:
            descError = 'Error no determinado, consulte con el administrador';
            break;
        }
        return {
          code: error.errorNum,
          description: descError,
        };
      });
  }

  async findCritical(value: string) {
    return await this.repositorio.createQueryBuilder('critical').select()
      .where('critical.aircraft like :name', { name: '%' + value + '%' })
      .getMany().then(lista => {
        return {
          response: {
            code: 0,
            description: 'Accion realizada con exito',
          },
          data: {
            critical_list: lista,
          },
        };
      }).catch(error => {
        Logger.verbose(`error => ${error}`);
        return {
          code: error.errorNum,
          description: 'Error no determinado, consulte con el administrador',
        };
      });
  }

  async getListCritical() {
    return await this.repositorio.createQueryBuilder().select().getMany().then(lista => {
      return {
        response: {
          code: 0,
          description: 'Accion realizada con exito',
        },
        data: {
          critical_list: lista,
        },
      };
    }).catch(error => {
      Logger.verbose(`error => ${error}`);
      return {
        code: error.errorNum,
        description: 'Error no determinado, consulte con el administrador',
      };
    });
  }
}
