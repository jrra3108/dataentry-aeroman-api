import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../../database/database.module';
import { CriticalController } from './controller/critical.controller';
import { CriticalService } from './service/critical.service';

@Module({
  imports: [DatabaseModule],
  controllers: [CriticalController],
  providers: [
    CriticalService],
})
export class CriticalModule {}
