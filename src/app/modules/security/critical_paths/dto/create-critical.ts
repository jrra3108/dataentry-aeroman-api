import { ApiModelProperty } from '@nestjs/swagger';

export class CreateCritical {
  @ApiModelProperty()
  id?: string;

  @ApiModelProperty()
  aircraft: string;

  @ApiModelProperty()
  workOrder: string;

  @ApiModelProperty()
  criticalPath: string;

  @ApiModelProperty()
  tatImpact: string;

  @ApiModelProperty()
  mitigationPlan: string;

  @ApiModelProperty()
  responsible: string;

  @ApiModelProperty()
  status: string;

  @ApiModelProperty()
  statusFinal: string;
}
