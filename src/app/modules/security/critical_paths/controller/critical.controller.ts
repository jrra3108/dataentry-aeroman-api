import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Body, Controller, Get, HttpCode, Post, Put, Delete, Param } from '@nestjs/common';
import { CriticalService } from '../service/critical.service';
import { CreateCritical } from '../dto/create-critical';

@ApiUseTags('Mantenimiento de Critical Paths')
@Controller('v1/manteiners/critical')
export class CriticalController {

  constructor(private readonly service: CriticalService) {}

  @ApiResponse({ status: 201, description: 'Creación de Critical Paths.' })
  @Post()
  @HttpCode(201)
  public async create(@Body() record: CreateCritical) {
    return this.service.create(record, 'abc-123');
  }

  @ApiResponse({ status: 201, description: 'Actualización de Critical Paths.' })
  @Put()
  @HttpCode(201)
  public async update(@Body() record: CreateCritical) {
    return this.service.update(record, '456789');
  }

  @ApiResponse({ status: 200, description: 'Lista de Critical Paths.' })
  @Get()
  @HttpCode(200)
  public async getCritical() {
    return this.service.getListCritical();
  }

  @ApiResponse({ status: 200, description: 'Búsqueda de Critical Paths.' })
  @Get('/find/:value')
  @HttpCode(200)
  public async find(@Param() params) {
    return this.service.findCritical(params.value);
  }

  @ApiResponse({ status: 200, description: 'Eliminar Critical Paths.' })
  @Delete(':id')
  @HttpCode(200)
  public async delete(@Param() params) {
    return this.service.delete(params.id);
  }

}
