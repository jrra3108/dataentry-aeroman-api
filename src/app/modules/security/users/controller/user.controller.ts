import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put } from '@nestjs/common';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { UserService } from '../service/user.service';
import { CreateUser } from '../dto/create-user';

@ApiUseTags('Mantenimiento de Usuarios')
@Controller('v1/seguridad/usuarios')
export class UserController {

  constructor(private readonly service: UserService) {
  }

  @ApiResponse({ status: 201, description: 'Creación de usuario.' })
  @Post()
  @HttpCode(201)
  public async create(@Body() record: CreateUser) {
    return this.service.create(record, 'abc-123');
  }

  @ApiResponse({ status: 201, description: 'Actualización de Usuarios.' })
  @Put()
  @HttpCode(201)
  public async update(@Body() record: CreateUser) {
    return this.service.update(record, '456789');
  }

  @ApiResponse({ status: 200, description: 'Lista de usuarios.' })
  @Get()
  @HttpCode(200)
  public async getUsers() {
    return this.service.getListUsers();
  }

  @ApiResponse({ status: 200, description: 'Búsqueda de Usuarios.' })
  @Get('/find/:value')
  @HttpCode(200)
  public async find(@Param() params) {
    return this.service.findUsers(params.value);
  }

  @ApiResponse({ status: 200, description: 'Eliminar Usuarios.' })
  @Delete(':id')
  @HttpCode(200)
  public async delete(@Param() params) {
    return this.service.delete(params.id);
  }

}
