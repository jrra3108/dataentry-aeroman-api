import { Inject, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BcryptService } from './bcrypt.service';
import { Users } from '../../../../database/entities/user';
import { CreateUser } from '../dto/create-user';

@Injectable()
export class UserService {
  constructor(
    @Inject('USER_REPOSITORY') private readonly repositorio: Repository<Users>,
  ) {
  }

  async create(registro: CreateUser, createdBy: string) {

    const passwordHash = await BcryptService.generateHash(registro.password);

    return await this.repositorio.createQueryBuilder().insert().into(Users).values([
      {
        username: registro.username,
        password: passwordHash,
        status: registro.status,
        createBy: createdBy,
      },
    ]).execute().then(() => {
      return {
        code: 0,
        description: 'Usuario creado con éxito',
      };
    }).catch(error => {
      Logger.verbose(`error => ${error}`);
      let descError;
      switch (error.errorNum) {
        case 1:
          descError = 'El usuario ya existe, el usuario y correo electrónico deben ser unicos.';
          break;
        case 1400:
          descError = 'No se pudo registrar el usuario, complete la información';
          break;
        default:
          descError = 'Error no determinado, consulte con el administrador';
          break;
      }
      return {
        code: error.errorNum,
        description: descError,
      };
    });
  }

  async update(registro: CreateUser, updateBy: string) {

    const passwordHash = await BcryptService.generateHash(registro.password);
    return await this.repositorio.createQueryBuilder().update(Users).set(
      {
        username: registro.username,
        password: passwordHash,
        status: registro.status,
        updatedBy: updateBy,
      },
    ).where('id = :id', { id: registro.id }).execute().then(() => {
      return {
        code: 0,
        description: 'Usuario actualizado con éxito',
      };
    }).catch(error => {
      Logger.verbose(`error => ${error}`);
      let descError;
      switch (error.errorNum) {
        case 1:
          descError = 'El usuario ya existe, el usuario y correo electrónico deben ser unicos.';
          break;
        case 1400:
          descError = 'No se pudo registrar el usuario, complete la información';
          break;
        default:
          descError = 'Error no determinado, consulte con el administrador';
          break;
      }
      return {
        code: error.errorNum,
        description: descError,
      };
    });
  }

  async delete(idrecord: string) {

    return await this.repositorio.createQueryBuilder().delete().from(Users)
      .where('id = :id', { id: idrecord }).execute().then(() => {
        return {
          code: 0,
          description: 'Usuario actualizado con éxito',
        };
      }).catch(error => {
        Logger.verbose(`error => ${error}`);
        let descError;
        switch (error.errorNum) {
          case 1:
            descError = 'El usuario ya existe.';
            break;
          case 1400:
            descError = 'No se pudo registrar el usuario, complete la información';
            break;
          default:
            descError = 'Error no determinado, consulte con el administrador';
            break;
        }
        return {
          code: error.errorNum,
          description: descError,
        };
      });
  }

  async findUsers(value: string) {
    return await this.repositorio.createQueryBuilder('user').select()
      .where('user.username like :name', { name: '%' + value + '%' })
      .getMany().then(lista => {
        return {
          response: {
            code: 0,
            description: 'Accion realizada con exito',
          },
          data: {
            lista_usuarios: lista,
          },
        };
      }).catch(error => {
        Logger.verbose(`error => ${error}`);
        return {
          code: error.errorNum,
          description: 'Error no determinado, consulte con el administrador',
        };
      });
  }

  async getListUsers() {
    return await this.repositorio.createQueryBuilder().select().getMany().then(lista => {
      return {
        response: {
          code: 0,
          description: 'Accion realizada con exito',
        },
        data: {
          lista_usuarios: lista,
        },
      };
    }).catch(error => {
      Logger.verbose(`error => ${error}`);
      return {
        code: error.errorNum,
        description: 'Error no determinado, consulte con el administrador',
      };
    });
  }

}
