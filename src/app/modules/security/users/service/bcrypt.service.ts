import { Injectable } from '@nestjs/common';

// const bcrypt = require('bcrypt');
const bcrypt = require ('bcryptjs');
const BCRYPT_SALT_ROUNDS = 15;

@Injectable()
export class BcryptService {

  public static generateHash(textPlain): Promise<string> {
    return new Promise<string>( (resolve, reject) => {
      bcrypt.hash(textPlain, BCRYPT_SALT_ROUNDS, (err, hash) => {
        if (err) {
          reject();
        }
        resolve(hash);
      });
    });
  }

  public static compareHash(textplain, passwordHash): Promise<boolean> {
    return new Promise<boolean>( ((resolve, reject) => {
      bcrypt.compare(textplain, passwordHash, (err, res) => {
        if (err) {
          reject();
        }
        resolve(res);
      });
    }));
  }
}
