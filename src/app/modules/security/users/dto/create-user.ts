import { ApiModelProperty } from '@nestjs/swagger';

export class CreateUser {
  @ApiModelProperty()
  id: string;

  @ApiModelProperty()
  username: string;

  @ApiModelProperty()
  password: string;

  @ApiModelProperty()
  status: string;
}
