import { DatabaseModule } from '../../../database/database.module';
import { Module } from '@nestjs/common';
import { DailyController } from './controller/daily.controller';
import { DailyService } from './service/daily.service';

@Module({
  imports: [DatabaseModule],
  controllers: [DailyController],
  providers: [
    DailyService],
})
export class DailyModule {
}
