import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put } from '@nestjs/common';
import { DailyService } from '../service/daily.service';
import { CreateDaily } from '../dto/create-daily';

@ApiUseTags('Mantenimiento de Daily Focus')
@Controller('v1/manteiners/daily')
export class DailyController {
  constructor(private readonly service: DailyService) {}

  @ApiResponse({ status: 201, description: 'Creación de Daily Focus.' })
  @Post()
  @HttpCode(201)
  public async create(@Body() record: CreateDaily) {
    return this.service.create(record, 'abc-123');
  }

  @ApiResponse({ status: 201, description: 'Actualización de Daily Focus.' })
  @Put()
  @HttpCode(201)
  public async update(@Body() record: CreateDaily) {
    return this.service.update(record, '456789');
  }

  @ApiResponse({ status: 200, description: 'Lista de Daily Focus.' })
  @Get()
  @HttpCode(200)
  public async getDaily() {
    return this.service.getListDaily();
  }

  @ApiResponse({ status: 200, description: 'Búsqueda de Daily Focus.' })
  @Get('/find/:value')
  @HttpCode(200)
  public async find(@Param() params) {
    return this.service.findDaily(params.value);
  }

  @ApiResponse({ status: 200, description: 'Eliminar Daily Focus.' })
  @Delete(':id')
  @HttpCode(200)
  public async delete(@Param() params) {
    return this.service.delete(params.id);
  }

}
