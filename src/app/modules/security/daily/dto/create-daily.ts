import { ApiModelProperty } from '@nestjs/swagger';

export class CreateDaily {
  @ApiModelProperty()
  id?: string;

  @ApiModelProperty()
  aircraft: string;

  @ApiModelProperty()
  workOrder: string;

  @ApiModelProperty()
  task: string;

  @ApiModelProperty()
  complete: number;

  @ApiModelProperty()
  reason: string;

  @ApiModelProperty()
  dateId: Date;
}
