import { Inject, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { DailyFocus } from '../../../../database/entities/daily-focus';
import { CreateDaily } from '../dto/create-daily';

@Injectable()
export class DailyService {
  constructor(
    @Inject('DAILY_REPOSITORY') private readonly repositorio: Repository<DailyFocus>,
  ) {}

  async create(registro: CreateDaily, createdBy: string) {

    return await this.repositorio.createQueryBuilder().insert().into(DailyFocus).values([
      {
        aircraft: registro.aircraft,
        workOrder: registro.workOrder,
        task: registro.task,
        complete: registro.complete,
        reason: registro.reason,
        dateId: registro.dateId,
        createBy: createdBy,
      },
    ]).execute().then(() => {
      return {
        code: 0,
        description: 'Daily creado con éxito',
      };
    }).catch(error => {
      Logger.verbose(`error => ${error}`);
      let descError;
      switch (error.errorNum) {
        case 1:
          descError = 'Daily ya existe.';
          break;
        case 1400:
          descError = 'No se pudo registrar la Opción principal de Menu, complete la información';
          break;
        default:
          descError = 'Error no determinado, consulte con el administrador';
          break;
      }
      return {
        code: error.errorNum,
        description: descError,
      };
    });
  }

  async update(registro: CreateDaily, createdBy: string) {

    return await this.repositorio.createQueryBuilder().update(DailyFocus).set(
      {
        aircraft: registro.aircraft,
        workOrder: registro.workOrder,
        task: registro.task,
        complete: registro.complete,
        reason: registro.reason,
        dateId: registro.dateId,
        createBy: createdBy,
      },
    ).where('id = :id', { id: registro.id }).execute().then(() => {
      return {
        code: 0,
        description: 'Daily actualizado con éxito',
      };
    }).catch(error => {
      Logger.verbose(`error => ${error}`);
      let descError;
      switch (error.errorNum) {
        case 1:
          descError = 'Daily ya existe.';
          break;
        case 1400:
          descError = 'No se pudo registrar el Daily, complete la información';
          break;
        default:
          descError = 'Error no determinado, consulte con el administrador';
          break;
      }
      return {
        code: error.errorNum,
        description: descError,
      };
    });
  }

  async delete(idrecord: string) {

    return await this.repositorio.createQueryBuilder().delete().from(DailyFocus)
      .where('id = :id', { id: idrecord }).execute().then(() => {
        return {
          code: 0,
          description: 'Empresa actualizada con éxito',
        };
      }).catch(error => {
        Logger.verbose(`error => ${error}`);
        let descError;
        switch (error.errorNum) {
          case 1:
            descError = 'La empresa ya existe.';
            break;
          case 1400:
            descError = 'No se pudo registrar la empresa, complete la información';
            break;
          default:
            descError = 'Error no determinado, consulte con el administrador';
            break;
        }
        return {
          code: error.errorNum,
          description: descError,
        };
      });
  }

  async findDaily(value: string) {
    return await this.repositorio.createQueryBuilder('daily').select()
      .where('daily.aircraft like :name', { name: '%' + value + '%' })
      .getMany().then(lista => {
        return {
          response: {
            code: 0,
            description: 'Accion realizada con exito',
          },
          data: {
            daily_list: lista,
          },
        };
      }).catch(error => {
        Logger.verbose(`error => ${error}`);
        return {
          code: error.errorNum,
          description: 'Error no determinado, consulte con el administrador',
        };
      });
  }

  async getListDaily() {
    return await this.repositorio.createQueryBuilder().select().getMany().then(lista => {
      return {
        response: {
          code: 0,
          description: 'Accion realizada con exito',
        },
        data: {
          daily_list: lista,
        },
      };
    }).catch(error => {
      Logger.verbose(`error => ${error}`);
      return {
        code: error.errorNum,
        description: 'Error no determinado, consulte con el administrador',
      };
    });
  }
}
