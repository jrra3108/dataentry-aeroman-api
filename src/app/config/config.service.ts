import * as dotenv from 'dotenv';
import * as fs from 'fs';
// tslint:disable-next-line:no-var-requires
const Joi = require('@hapi/joi');


export interface EnvConfig {
  [key: string]: string;
}

export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor(filePath: string) {
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = this.validateInput(config);
  }

  getConfigDatabase() {
    return {
      TYPE: this.envConfig.DATABASE_TYPE,
      HOST: this.envConfig.DATABASE_HOST,
      PORT: this.envConfig.DATABASE_PORT,
      SID: this.envConfig.DATABASE_SID,
      SCHEMA: this.envConfig.DATABASE_SCHEMA,
      USER: this.envConfig.DATABASE_USER,
      PASSWORD: this.envConfig.DATABASE_PASSWORD,
    };
  }

  getUrlApi() {
    return this.envConfig.URL_API;
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema = Joi.object().keys({
      NODE_ENV: Joi.string().valid(['development', 'production', 'test', 'provision']).default('development'),
      PORT: Joi.number().default(3000),
      DATABASE_HOST: Joi.string().default('localhost'),
      DATABASE_PORT: Joi.string().required(),
      DATABASE_SID: Joi.string(),
      DATABASE_USER: Joi.string().default('userdemo'),
      DATABASE_PASSWORD: Joi.string().default('userdemo'),
      DATABASE_SCHEMA: Joi.string().default('public'),
      URL_API: Joi.string().default('http://balancer/api/v1'),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }
}
