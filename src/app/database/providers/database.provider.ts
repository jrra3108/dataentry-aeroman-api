import { ConfigService } from '../../config/config.service';
import { Connection, createConnection } from 'typeorm';
import { Users } from '../entities/user';
import { CriticalPaths } from '../entities/critical-paths';
import { DailyFocus } from '../entities/daily-focus';

export const DatabaseProvider = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () => {
      const config = new ConfigService('.env');
      const configDatabase = config.getConfigDatabase();
      return createConnection({
        type: 'oracle',
        host: configDatabase.HOST,
        port: Number(configDatabase.PORT),
        username: configDatabase.USER,
        password: configDatabase.PASSWORD,
        sid: configDatabase.SID,
        // schema: configDatabase.SCHEMA,
        entities: [
          Users,
          CriticalPaths,
          DailyFocus,
          ],
        synchronize: true,
        logging: true,
        subscribers: [],
      });
    },
  },
  {
    provide: 'USER_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Users),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'CRITICAL_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(CriticalPaths),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'DAILY_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(DailyFocus),
    inject: ['DATABASE_CONNECTION'],
  },
];
