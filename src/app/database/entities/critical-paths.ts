import {
  BeforeInsert, BeforeUpdate, Column,
  CreateDateColumn,
  Entity, OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

@Entity('CRITICAL_PATHS')
export class CriticalPaths {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  aircraft: string;

  @Column()
  workOrder: string;

  @Column()
  criticalPath: string;

  @Column()
  tatImpact: string;

  @Column()
  mitigationPlan: string;

  @Column()
  responsible: string;

  @Column({ type: 'char', length: 1})
  status: string;

  @Column({ type: 'char', length: 1})
  statusFinal: string;

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  UpdatedBy: string;

  @VersionColumn() version: number;

  /* Eventos */
  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }

}
