import {
  BeforeInsert, BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity, ManyToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

@Entity('USERS')
export class Users {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({unique: true})
  username: string;

  @Column()
  password: string;

  @Column({ type: 'char', length: 1})
  status: string;

  @CreateDateColumn({ nullable: false })
  createAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @VersionColumn() version: number;

  /* Eventos */
  @BeforeInsert()
  insertDates() {
    this.createAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
