import {
  BeforeInsert, BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

@Entity('DAILY_FOCUS')
export class DailyFocus {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  aircraft: string;

  @Column()
  workOrder: string;

  @Column()
  task: string;

  @Column()
  complete: number;

  @Column()
  reason: string;

  @Column()
  dateId: Date;

  @CreateDateColumn({ nullable: false })
  createAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  UpdatedBy: string;

  @VersionColumn() version: number;

  /* Eventos */
  @BeforeInsert()
  insertDates() {
    this.createAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }

}
