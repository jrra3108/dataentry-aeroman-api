export class DbErrorsList {

  public getError(code): {description: string} {
    switch (code) {
      case 1:
        return {
          description: 'Registro duplicado',
        };
      case 1400:
        return {
          description: 'Registro incompleto',
        };
      default:
        return {
          description: 'Error no determinado',
        };
    }
  }
}
