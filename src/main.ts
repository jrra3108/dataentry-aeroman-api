import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './http.exception.filter';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  app.enableCors();
  app.useGlobalFilters(new HttpExceptionFilter());
  const options = new DocumentBuilder()
    .setTitle('ERP-SIF')
    .setDescription('Sistema de Gestión Financiera')
    .setVersion('1.0')
    .setBasePath(`/${globalPrefix}`)
    .setContactEmail('jrra.3108@gmail.com')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('doc', app, document);

  const port = process.env.port || 3000;
  await app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });
}

bootstrap();
