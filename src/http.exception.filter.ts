import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus, UnsupportedMediaTypeException } from '@nestjs/common';

@Catch(UnsupportedMediaTypeException)
export class HttpExceptionFilter implements ExceptionFilter {

  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    response.status(status).json({
      statusCode: status,
      message: 'No fue posible procesar su peticion',
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
