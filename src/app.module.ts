import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './app/database/database.module';
import { UserModule } from './app/modules/security/users/user.module';
import { CriticalModule } from './app/modules/security/critical_paths/critical.module';
import { DailyModule } from './app/modules/security/daily/daily.module';
import { AuthModule } from './app/auth/auth.module';

@Module({
  imports: [
    DatabaseModule,
    UserModule,
    CriticalModule,
    DailyModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
